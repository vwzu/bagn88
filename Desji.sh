  - uses: actions/checkout@v2
      - uses: actions/download-artifact@v2
        with:
          name: cache
          path: ./cache
      - name: load base
        run: docker load -i ./cache/base.tar
      - name: build
        run: make ${{ matrix.arch_name }}
      - name: test
        run: make ${{ matrix.arch_name }}.test
      - name: deploy
        if: github.ref == 'refs/heads/master'
        run: |
            docker login -u ${{ secrets.DOCKER_USER }} -p ${{ secrets.DOCKER_PASS }}
            docker image push d
